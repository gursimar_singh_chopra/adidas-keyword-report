import json,sys,csv,codecs,requests


def WriteHeaders(headers):
	cfile =open(sys.argv[1] + ".csv",'w+')
	csvfile = csv.writer(cfile)
	csvfile.writerow(headers)
	return csvfile

def hande_encode_data(value):
	nvalue=''
	try:
 	       nvalue=str(value)
	except Exception,e:
	        navlue=''
	return nvalue

br_list = ["adidas","reebok","nike","asics","skecher","puma","adidas neo","Nike Kids","Nike Air","Nike Sb","Nike Sportswear","Reebok Classic","Reebok Work"]

def generate_report(file1,fp):
    for data in open(file1):
        towrite=[]
        ldata = json.loads(data)
        towrite.append(ldata.get('source','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('sku','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('keyword','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('seed_url','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('rank','NA'))
        towrite.append(ldata.get('title','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('brand_tagged','NA').encode('ascii', 'ignore').strip())
        if ldata.get('brand_tagged','NA').lower() in br_list:
            towrite.append(ldata.get('brand_tagged','NA').encode('ascii', 'ignore').strip().title())
        else:
            towrite.append('Others')
        towrite.append(ldata.get('mrp','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('available_price','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('stock','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('url','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('thumbnail','NA').encode('ascii', 'ignore').strip())
        if ldata['seed_url'].replace('https:','http:') == ldata['pagination_url'].replace('https:','http:'):
            towrite.append('Yes')
        else:
            towrite.append('No')
        towrite.append(ldata.get('crawl_date','NA').encode('ascii', 'ignore').strip())
        towrite.append(ldata.get('crawl_time','NA').encode('ascii', 'ignore').strip())
        towrite.append("http://api.priceweave.com/cache/?urlh="+ldata['pagination_urlh']+"&crawl_type=listing&crawl_time="+ldata['crawl_time'])
        tmpwrite = []
        for listdata in towrite:
            tmpwrite.append(hande_encode_data(listdata))
        fp.writerow(tmpwrite)
    r = requests.post('http://api.priceweave.com/mail/mailer2.php?mail_parameter={"to":"amar@dataweave.com", "from":"reports@dataweave.com","subject":"Adidas Best Seller KeyWord Report","body":"Hi PFA , Here is the weekly keyword best seller report"}', files={'Adidas.csv': open('/home/DEV/Adidas/Scripts/Adidas.csv', 'rb')})

if  __name__ == '__main__':
	input_file = sys.argv[1]
	log = open(input_file+"__Excepthions.json",'w+')
	headers=["Source","Sku","KeyWord","Search_URL","Rank","Title","Brand","Brand2","MRP","Available Price","Stock","URL","Thumbnail","Page1","Crawl_Date","Crawl_Time","Cache_API"]
	fp=WriteHeaders(headers)
	generate_report(input_file,fp)

