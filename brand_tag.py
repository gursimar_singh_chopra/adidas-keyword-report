import requests, sys, json

input_file_name = sys.argv[1]
output_file_name = input_file_name + ".output_brand_tagged"

fw = open(output_file_name, "w")

for data in open(input_file_name):
    ldata = json.loads(data)
    try:
	print(ldata.get('title','NA'))
        if ldata.get('title', 'NA').find(ldata.get('brand', 'NA')) == -1:
            if ldata.get('brand', 'NA') != 'NA':
                new_title = ldata.get('brand', '') + ' ' + ldata.get('title', '')
                ldata['title'] = new_title
            print("brand not found")
            pass
                 
        url = "http://api.tagging.dweave.net/attribute_tagging?docs=[{%22title%22:%22" + ldata.get('title','') + "%22}]&attributes=brand"
        resp = requests.get(url).json()
        ldata['brand_tagged'] = resp['response'][0]['attribute_api_tagging']['brand_tagging']
        print ldata['brand_tagged']
    except:
        ldata['brand_tagged'] = 'Others'
    fw.write(json.dumps(ldata) + '\n')
