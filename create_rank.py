import sys
import json
import datetime
import time
final_dict={}
todays_date = datetime.datetime.now().strftime("%Y%m%d")
f=open(sys.argv[1]+todays_date+".json","w+")
def createDict(file):
    for line in open(file):
        data=json.loads(line)
        data['uid'] = data['source']+data['keyword']
        seed_urlh=data.get("uid","NA")
        if seed_urlh not in final_dict:
            final_dict[seed_urlh]={}
            final_dict[seed_urlh]["data"]=[]
        final_dict[seed_urlh]["data"].append(data)
    return final_dict

def createRank(final_dict):
    for key,value in final_dict.iteritems():
	    print key
	    rank=0
	    for d in value.get("data"):
		    rank+=1
		    d["rank"]=str(rank)
		    print >> f,json.dumps(d)
if __name__ == "__main__":
    filename = sys.argv[1]
    dict = createDict(filename)
    createRank(dict)
